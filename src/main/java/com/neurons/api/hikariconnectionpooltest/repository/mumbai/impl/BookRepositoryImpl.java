
package com.neurons.api.hikariconnectionpooltest.repository.mumbai.impl;

import com.neurons.api.hikariconnectionpooltest.model.mumbai.Book;
import com.neurons.api.hikariconnectionpooltest.model.mumbai.BookAuthor;
import com.neurons.api.hikariconnectionpooltest.repository.mumbai.BookRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class BookRepositoryImpl implements BookRepository {

    @PersistenceContext(name = "mumbaiEntityManagerFactory")
    private EntityManager em;

    @Override
    public List<Book> getBooksList() {

      List<Book> books=em.createQuery("from Book").getResultList();
        return books;
    }


    @Override
    public List<Book> addBook(Book inBook) {
      if(em.find(Book.class,inBook.getBookId())==null){
            em.persist(inBook);
        }
        else{
            em.merge(inBook);
        }

        return getBooksList();
    }

    @Override
    public List<BookAuthor> getBooksListWithAuthor() {

        Query query = em.createNativeQuery("select b.bookId,a.authorId,b.bookName,a.authorName," +
                "a.authorAddress,price from book b inner join author a on b.authorId=a.authorId");
        @SuppressWarnings("unchecked")
        List<Object[]> results = (List<Object[]>) query.getResultList();

        List<BookAuthor> bookAuthorList= new ArrayList<>();

        results.forEach(data -> {
            BookAuthor bookAuthor= new BookAuthor();
            bookAuthor.setBookId((Integer) (data[0]));
            bookAuthor.setAuthorId((Integer)(data[1]));
            bookAuthor.setBookName((String) data[2]);
            bookAuthor.setAuthorName((String) data[3]);
            bookAuthor.setAuthorAddress((String) data[4]);
            bookAuthor.setPrice((float) data[5]);
            bookAuthorList.add(bookAuthor);
        });

        return bookAuthorList;
    }
}

