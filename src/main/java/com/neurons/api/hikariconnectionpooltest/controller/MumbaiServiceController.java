package com.neurons.api.hikariconnectionpooltest.controller;

import com.neurons.api.hikariconnectionpooltest.model.mumbai.Book;
import com.neurons.api.hikariconnectionpooltest.service.MumbaiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MumbaiServiceController {

    @Autowired
    MumbaiService mumbaiService;


    @GetMapping(path = "/book",produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity getBookList(){

        return ResponseEntity.ok(mumbaiService.getBooks());
    }


    @PostMapping(path = "/book", produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity getBookList(@RequestBody(required = true) Book inBook){

        return ResponseEntity.ok(mumbaiService.addBook(inBook));
    }

    @GetMapping(path = "/bookAuthor",produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity getBookAuthorList(){

        return ResponseEntity.ok(mumbaiService.getBooksWithAuthor());
    }

    @GetMapping(path = "/author",produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity getAuthorList(){

        return ResponseEntity.ok(mumbaiService.getAuthors());
    }

}
