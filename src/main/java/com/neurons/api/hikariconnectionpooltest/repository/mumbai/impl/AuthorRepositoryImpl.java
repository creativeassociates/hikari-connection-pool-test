package com.neurons.api.hikariconnectionpooltest.repository.mumbai.impl;

import com.neurons.api.hikariconnectionpooltest.model.mumbai.Author;
import com.neurons.api.hikariconnectionpooltest.repository.mumbai.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


@SuppressWarnings("unchecked")
@Service
public class AuthorRepositoryImpl implements AuthorRepository {

    @Autowired
    DataSource mumbaiHikariDataSource;

    JdbcTemplate jdbcTemplate;

    @Override
    public List<Author> getAuthors() {

            List<Author> authors = getJdbcTemplate().query("select * from author", new AuthorRowMapper());
            return authors;
    }

    private JdbcTemplate getJdbcTemplate() {

        if (jdbcTemplate == null) {
            jdbcTemplate = new JdbcTemplate(mumbaiHikariDataSource);
        }
        return jdbcTemplate;
    }


    class AuthorRowMapper implements RowMapper<Author> {

        @Override
        public Author mapRow(ResultSet rs, int rowNum) throws SQLException {

            Author author = new Author();
            author.setAuthorId(rs.getInt("authorId"));
            author.setAuthorName(rs.getString("authorName"));
            author.setAuthorAddress(rs.getString("authorAddress"));
            return author;
        }
    }

}