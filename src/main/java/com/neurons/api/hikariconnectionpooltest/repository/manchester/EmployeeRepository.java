package com.neurons.api.hikariconnectionpooltest.repository.manchester;

import com.neurons.api.hikariconnectionpooltest.model.manchester.Employee;
import org.springframework.data.repository.CrudRepository;


public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
}
