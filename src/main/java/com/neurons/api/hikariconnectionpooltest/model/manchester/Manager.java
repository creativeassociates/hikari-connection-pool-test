package com.neurons.api.hikariconnectionpooltest.model.manchester;


import javax.persistence.*;

@Entity
@Table(name = "manager")
public class Manager {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "managerId")
    private int managerId;

    @Column(name = "managerName")
    private String managerName;

    @Column(name = "designation")
    private String designation;

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }
}
