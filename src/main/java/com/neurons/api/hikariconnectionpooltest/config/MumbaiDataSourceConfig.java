package com.neurons.api.hikariconnectionpooltest.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;

//@PropertySource("classpath:application.properties")
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "mumbaiEntityManagerFactory",
        transactionManagerRef = "mumbaiTransactionManager", basePackages = {"com.neurons.api.hikariconnectionpooltest.repository.mumbai"})
public class MumbaiDataSourceConfig {

    @Primary
    @Bean("mumbaiHikariConfig")
    @ConfigurationProperties(prefix = "mumbai.datasource.hikari")
    public HikariConfig hikariConfig() {
        return new HikariConfig();
    }

    @Primary
    @Bean(name = "mumbaiHikariDataSource")
    public DataSource dataSource(final @Qualifier("mumbaiHikariConfig") HikariConfig mumbaiHikariConfig) {
        return new HikariDataSource(hikariConfig());
    }

@Primary
    @Bean(name = "mumbaiEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean DS1EntityManagerFactory(final @Qualifier("mumbaiHikariDataSource") DataSource mumbaiHikariDataSource) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(mumbaiHikariDataSource);
        em.setPersistenceUnitName("mumbai");
        em.setPackagesToScan("com.neurons.api.hikariconnectionpooltest.model.mumbai");
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        return em;
    }

    @Primary
    @Bean(name = "mumbaiTransactionManager")
    public PlatformTransactionManager DS1TransactionManager(final @Qualifier("mumbaiEntityManagerFactory") LocalContainerEntityManagerFactoryBean memberEntityManagerFactory) {
        JpaTransactionManager transactionManager
                = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(memberEntityManagerFactory.getObject());
        return transactionManager;
    }

}
