package com.neurons.api.hikariconnectionpooltest.service.impl;

import com.neurons.api.hikariconnectionpooltest.model.paris.Student;
import com.neurons.api.hikariconnectionpooltest.repository.paris.StudentRepository;
import com.neurons.api.hikariconnectionpooltest.service.ParisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ParisServiceImpl implements ParisService {

    @Autowired
    StudentRepository studentRepository;


    @Override
    public List<Student> getStudents() {

    List<Student> students = new ArrayList<>();
    studentRepository.findAll().forEach(students::add);
    return students;
    }
}
