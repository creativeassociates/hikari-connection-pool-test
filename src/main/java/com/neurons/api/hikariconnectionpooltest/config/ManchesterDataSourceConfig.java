package com.neurons.api.hikariconnectionpooltest.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;

//@PropertySource("classpath:application.properties")
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "manchesterEntityManagerFactory",
        transactionManagerRef = "manchesterTransactionManager", basePackages = {"com.neurons.api.hikariconnectionpooltest.repository.manchester"})
public class ManchesterDataSourceConfig {

    @Bean("manchesterHikariConfig")
    @ConfigurationProperties(prefix = "manchester.datasource.hikari")
    public HikariConfig hikariConfig() {
        return new HikariConfig();
    }

    @Bean(name = "manchesterHikariDataSource")
    public DataSource dataSource(final @Qualifier("manchesterHikariConfig") HikariConfig manchesterHikariConfig) {
        return new HikariDataSource(hikariConfig());
    }


    @Bean(name = "manchesterEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean DS1EntityManagerFactory(final @Qualifier("manchesterHikariDataSource") DataSource manchesterHikariDataSource) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(manchesterHikariDataSource);
        em.setPersistenceUnitName("manchester");
        em.setPackagesToScan("com.neurons.api.hikariconnectionpooltest.model.manchester");
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        return em;
    }


    @Bean(name = "manchesterTransactionManager")
    public PlatformTransactionManager DS1TransactionManager(final @Qualifier("manchesterEntityManagerFactory") LocalContainerEntityManagerFactoryBean memberEntityManagerFactory) {
        JpaTransactionManager transactionManager
                = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(memberEntityManagerFactory.getObject());
        return transactionManager;
    }

}
