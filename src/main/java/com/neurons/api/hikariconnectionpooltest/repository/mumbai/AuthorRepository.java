package com.neurons.api.hikariconnectionpooltest.repository.mumbai;

import com.neurons.api.hikariconnectionpooltest.model.mumbai.Author;

import java.util.List;

public interface AuthorRepository {

    List<Author> getAuthors();
}
