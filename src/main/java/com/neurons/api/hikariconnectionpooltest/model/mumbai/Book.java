package com.neurons.api.hikariconnectionpooltest.model.mumbai;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "book")
public class Book {

    @Id
    //@GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "bookId")
    private int bookId;

    @Column(name="authorId")
    private int authorId;

    @Column(name = "bookName")
    private String bookName;

    @Column(name = "price")
    private String price;

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}

