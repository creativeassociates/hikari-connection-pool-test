package com.neurons.api.hikariconnectionpooltest.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class TestController {

    @GetMapping(path = "/testCookie")
    public ResponseEntity<?> addCookie(HttpServletRequest request, HttpServletResponse response) {


        Map<String,String> cookieValMap= new HashMap<>();
        cookieValMap.put("Authorization",UUID.randomUUID().toString());
        cookieValMap.put("Max-Age","-1");
        cookieValMap.put("Path","/");
        cookieValMap.put("Secure","");
        cookieValMap.put("Domain","localhost");
        cookieValMap.put("HttpOnly","");

        String cookie =cookieValMap
                .keySet()
                .stream()
                .map(s -> s+"="+cookieValMap.get(s))
                .collect(Collectors.joining("; "));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Set-Cookie",cookie);
        return ResponseEntity.ok().headers(headers).body("Success");
    }
}
