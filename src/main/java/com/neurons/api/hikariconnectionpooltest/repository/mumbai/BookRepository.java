package com.neurons.api.hikariconnectionpooltest.repository.mumbai;

import com.neurons.api.hikariconnectionpooltest.model.mumbai.Book;
import com.neurons.api.hikariconnectionpooltest.model.mumbai.BookAuthor;

import java.util.List;


//public interface BookRepository extends CrudRepository<Book, Integer> {
//}
public interface BookRepository {
 List<Book> getBooksList();

 List<Book> addBook(Book book);

 List<BookAuthor> getBooksListWithAuthor();
}
