package com.neurons.api.hikariconnectionpooltest.service.impl;

import com.neurons.api.hikariconnectionpooltest.model.mumbai.Author;
import com.neurons.api.hikariconnectionpooltest.model.mumbai.Book;
import com.neurons.api.hikariconnectionpooltest.model.mumbai.BookAuthor;
import com.neurons.api.hikariconnectionpooltest.repository.mumbai.AuthorRepository;
import com.neurons.api.hikariconnectionpooltest.repository.mumbai.BookRepository;
import com.neurons.api.hikariconnectionpooltest.service.MumbaiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MumbaiServiceImpl implements MumbaiService {

    @Autowired
    BookRepository bookRepository;

    @Autowired
    AuthorRepository authorRepository;


    @Override
    public List<Book> getBooks() {

    List<Book> books =bookRepository.getBooksList();
    return books;
    }

    @Override
    public List<Book> addBook(Book inBook) {
        return bookRepository.addBook(inBook);
    }

    @Override
    public List<BookAuthor> getBooksWithAuthor() {
        return bookRepository.getBooksListWithAuthor();
    }

    @Override
    public List<Author> getAuthors() {
        return authorRepository.getAuthors();
    }
}
