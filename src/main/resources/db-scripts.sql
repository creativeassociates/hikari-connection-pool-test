
create database manchester;
create database paris;
create database mumbai;

use manchester;


create table employee
(
employeeId int not null,
employeeName varchar(500),
designation varchar(100)
);


insert into  employee values(1,'om prakash','SA');
insert into  employee values(2,'vijay kumar','A');

select * from employee;


create table manager
(
managerId int not null,
managerName varchar(500),
designation varchar(100)
);

insert into  manager values(1,'allem','M');
insert into  manager values(2,'rikin','SM');

select * from manager;


use paris;

create table student
(
studentId int not null,
studentName varchar(500),
grade varchar(100)
);

insert into  student values(1,'om prakash','A');
insert into  student values(2,'vijay kumar','B');

select * from student;

use mumbai;
create table book
(
bookId int not null,
bookName varchar(500),
price float
);

insert into  book values(1,'The Complete Reference','5000');
insert into  book values(2,'Python in 30 Day','3000');

show processlist;


SHOW STATUS WHERE `variable_name` = 'Threads_connected';