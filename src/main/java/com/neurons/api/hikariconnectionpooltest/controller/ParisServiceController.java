package com.neurons.api.hikariconnectionpooltest.controller;

import com.neurons.api.hikariconnectionpooltest.service.ParisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ParisServiceController {

    @Autowired
    ParisService parisService;


    @GetMapping(path = "/student",produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity getStudentList(){

        return ResponseEntity.ok(parisService.getStudents());
    }
}
