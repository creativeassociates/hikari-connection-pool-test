package com.neurons.api.hikariconnectionpooltest.controller;


import com.neurons.api.hikariconnectionpooltest.service.ManchesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ManchesterController {

@Autowired
ManchesterService employeeService;

    @GetMapping(path = "/employee",produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity getEmployeeList(){

        return ResponseEntity.ok(employeeService.getEmployees());
    }

    @GetMapping(path = "/manager",produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity getManagerList(){

        return ResponseEntity.ok(employeeService.getManagers());
    }

}
