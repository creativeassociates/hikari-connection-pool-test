package com.neurons.api.hikariconnectionpooltest.repository.manchester;

import com.neurons.api.hikariconnectionpooltest.model.manchester.Manager;
import org.springframework.data.repository.CrudRepository;


public interface ManagerRepository extends CrudRepository<Manager, Integer> {
}
