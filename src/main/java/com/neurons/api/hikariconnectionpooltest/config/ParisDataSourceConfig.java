package com.neurons.api.hikariconnectionpooltest.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;

//@PropertySource("classpath:application.properties")
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "parisEntityManagerFactory",
        transactionManagerRef = "parisTransactionManager", basePackages = {"com.neurons.api.hikariconnectionpooltest.repository.paris"})
public class ParisDataSourceConfig {

    @Bean("parisHikariConfig")
    @ConfigurationProperties(prefix = "paris.datasource.hikari")
    public HikariConfig hikariConfig() {
        return new HikariConfig();
    }

    @Bean(name = "parisHikariDataSource")
    public DataSource dataSource(final @Qualifier("parisHikariConfig") HikariConfig parisHikariConfig) {
        return new HikariDataSource(hikariConfig());
    }


    @Bean(name = "parisEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean DS1EntityManagerFactory(final @Qualifier("parisHikariDataSource") DataSource parisHikariDataSource) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(parisHikariDataSource);
        em.setPersistenceUnitName("paris");
        em.setPackagesToScan("com.neurons.api.hikariconnectionpooltest.model.paris");
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        return em;
    }


    @Bean(name = "parisTransactionManager")
    public PlatformTransactionManager DS1TransactionManager(final @Qualifier("parisEntityManagerFactory") LocalContainerEntityManagerFactoryBean memberEntityManagerFactory) {
        JpaTransactionManager transactionManager
                = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(memberEntityManagerFactory.getObject());
        return transactionManager;
    }

}
