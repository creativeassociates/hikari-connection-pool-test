package com.neurons.api.hikariconnectionpooltest.repository.paris;

import com.neurons.api.hikariconnectionpooltest.model.paris.Student;
import org.springframework.data.repository.CrudRepository;


public interface StudentRepository extends CrudRepository<Student, Integer> {
}
