package com.neurons.api.hikariconnectionpooltest.service;

import com.neurons.api.hikariconnectionpooltest.model.manchester.Employee;
import com.neurons.api.hikariconnectionpooltest.model.manchester.Manager;

import java.util.List;

public interface ManchesterService {

    List<Employee> getEmployees();

    List<Manager> getManagers();

}
