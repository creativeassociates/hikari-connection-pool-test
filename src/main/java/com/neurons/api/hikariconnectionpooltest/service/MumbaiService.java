package com.neurons.api.hikariconnectionpooltest.service;

import com.neurons.api.hikariconnectionpooltest.model.mumbai.Author;
import com.neurons.api.hikariconnectionpooltest.model.mumbai.Book;
import com.neurons.api.hikariconnectionpooltest.model.mumbai.BookAuthor;

import java.util.List;

public interface MumbaiService {
    List<Book> getBooks();
    List<Book> addBook(Book inBook);
    List<BookAuthor> getBooksWithAuthor();
    List<Author> getAuthors();
}
