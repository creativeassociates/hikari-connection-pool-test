package com.neurons.api.hikariconnectionpooltest.service;

import com.neurons.api.hikariconnectionpooltest.model.paris.Student;

import java.util.List;

public interface ParisService {
    List<Student> getStudents();
}
