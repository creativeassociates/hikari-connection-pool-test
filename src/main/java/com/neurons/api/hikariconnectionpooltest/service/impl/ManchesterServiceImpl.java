package com.neurons.api.hikariconnectionpooltest.service.impl;

import com.neurons.api.hikariconnectionpooltest.model.manchester.Employee;
import com.neurons.api.hikariconnectionpooltest.model.manchester.Manager;
import com.neurons.api.hikariconnectionpooltest.repository.manchester.EmployeeRepository;
import com.neurons.api.hikariconnectionpooltest.repository.manchester.ManagerRepository;
import com.neurons.api.hikariconnectionpooltest.service.ManchesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ManchesterServiceImpl implements ManchesterService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    ManagerRepository managerRepository;

    @Override
    public List<Employee> getEmployees() {
        List<Employee> employees = new ArrayList<>();

        employeeRepository.findAll().forEach(employees::add);
        return  employees;
    }

    @Override
    public List<Manager> getManagers() {
        List<Manager> managers = new ArrayList<>();

        managerRepository.findAll().forEach(managers::add);
        return  managers;
    }
}
